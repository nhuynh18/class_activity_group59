'''
Created on Oct 2, 2019

@author: nhuynh18
'''
import unittest
from geeksforgeeks.algorithm import gcd


class gcdTest(unittest.TestCase):
    def test(self):
        
        a = 1
        b = 5 
        c = 6
        d = 5
        self.assertEqual(a, gcd(31,2))
        self.assertEqual(b, gcd(35,10))
        self.assertEqual(c, gcd(6,12))
        self.assertEqual(d, gcd(0,5))
        
       
       

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()