'''
Created on October 2, 2019

@author: Suchith Suddala <Suchiths>
@attention: this is test file for algorithm 
'''
import unittest
from geeksforgeeks.algorithm import gcd


class gcdTest(unittest.TestCase):
    def test(self):
        #declare needed variables for testing    
        a = 0
        b = 10
        c = 15
        d = 5
        e = 8
        f = 24
        g = 36
        
        #assert statements
        #self.assertEqual(<function call>,<expected output>)
        self.assertEqual(gcd(a, b), b)
        self.assertEqual(gcd(b, c), 5)
        self.assertEqual(gcd(c, d), 5)
        self.assertEqual(gcd(e, d), 1)
        self.assertEqual(gcd(f, g), 12)
        self.assertEqual(gcd(g, a), g)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()