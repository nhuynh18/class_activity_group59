'''

@author: aciszek
@attention: this is a template
'''
import unittest
from geeksforgeeks.algorithm import gcd


class gcdTest(unittest.TestCase):
    def test(self):
        ans = gcd(10, 15);
        self.assertTrue(ans == 5);
        
        ans = gcd(35, 10);
        self.assertTrue(ans == 5);
        
        ans = gcd(31, 2);
        self.assertEquals(ans, 1);
        
        ans = gcd(0, 10);
        self.assertTrue(ans == 10);

        ans = gcd(0, 0);
        self.assertTrue(ans == 0);
       
        ans = gcd(53, 0);
        self.assertEquals(ans, 53);

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()